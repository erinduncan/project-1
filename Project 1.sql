create table status(
    "statusId" serial primary key,
    status text
);

drop table status cascade;

create table type(
    "typeId" serial primary key,
    type text
);

drop table type cascade;

create table roles(
    "roleId" serial primary key,
    user_roles text
);

drop table roles cascade;

create table users(
    "userId" serial primary key,
    "username" text unique,
    "password" text,
    "firstName" text,
    "lastName" text,
    "email" text unique,
    "roleId" integer references roles ("roleId") on delete cascade
);

drop table users cascade;
truncate table users;

create table reimbursement(
    "reimbId" serial primary key,
    amount numeric(8,2),
    submitted timestamp,
    resolved timestamp,
    description text,
    author integer references users ("userId") on delete cascade,
    resolver integer references users ("userId") on delete cascade,
    "statusId" integer references status ("statusId") on delete cascade,
    "typeId" integer references type ("typeId") on delete cascade
);

drop table reimbursement cascade;

insert into status (status)
values ('pending'), ('approved'),('denied');
insert into roles (user_roles)
values ('employee'), ('manager');
insert into type (type)
values ('lodging'), ('travel'), ('food'), ('other');


create or replace procedure insert_new_employee(username text, password text, fname text, lname text, email text) language plpgsql as $$
	begin
		insert into users (username, password, first_name, last_name, email) values (username, password, fname, lname, email);
	commit;
	end;
$$;

create or replace function hashPassword()
returns trigger
as $$
begin
if(new.password=old.password)then
return new;
end if;
new.password :=
md5(new.username||new.password||'david');
return new;
end;
$$ language plpgsql;

create trigger hashPass
before insert or update on users
for each row
execute function hashPassword();

create or replace function validate(usern text, pass text) returns boolean as $$
  declare
  	hashp text;
  	savedhp text;
  begin
  	hashp := md5(usern||pass||'david');
  	select users."password" into savedhp from users where users."username" =usern;
  	if(hashp = savedhp) then return true;
  	else return false;
  end if;
  end
$$ language plpgsql;

insert into users (username, password, "firstName", "lastName", email, "roleId") values ('greggygoo', 'hordor', 'Greg','Hotch', 'ghotch@gmail.com', 1);
insert into users (username, password, "firstName", "lastName", email, "roleId") values ('gaaranimals', 'sandysnatch', 'Gaara','???', 'gaaranimals@gmail.com', 1);

insert into users (username, password, "firstName", "lastName", email, "roleId") values ('nuto', 'sipuden', 'Naruto','Uzamaki', 'narumaki@gmail.com', 2);

select validate('greggygoo','hordor');

select validate('nuto','sipuden');

select * from users;

SELECT users.username, reimbursement.resolver
FROM users
INNER JOIN reimbursement ON users."userId"=reimbursement."resolver";

SELECT users.username, reimbursement.author
FROM users
INNER JOIN reimbursement ON users."userId"=reimbursement."author";

