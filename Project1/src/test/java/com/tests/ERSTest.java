package com.tests;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.revature.dao.ReimbImpl;
import com.revature.dao.UserImpl;
import com.revature.model.Reimb;
import com.revature.model.User;
import com.revature.util.ConnectionUtil;

public class ERSTest {
	
	private User u;
	private UserImpl ui;

	
	@Before
	public void setup() {
		u = new User();
		ui = new UserImpl();
	}
	
	@Test
	public void insert() {
		ui.createUser(new User("a","b","c","d","f",1));
		System.out.println(ui.getUserByUsername("a"));
		assertTrue(u != null);
	}
	
	
	@Test
	public void connect() {
		assertNotNull(ConnectionUtil.connect());
	}
	
	
	@Test
	public void validateLogin() {
		String username = "bobert";
		String password = "rolltide";
		boolean result = ui.validateLogin(username, password);
		assertTrue(result);
	}
	
	
	public void validateSession() {
		String username = "bobert";
		String password = "rolltide";
		boolean result = ui.validateLogin(username, password);
	}



}
