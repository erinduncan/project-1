package com.revature.controller;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.revature.dao.UserImpl;
import com.revature.model.CurrentUser;
import com.revature.model.User;
import com.revature.service.UserService;

@SuppressWarnings("serial")
public class LoginController extends HttpServlet {

	private static Logger log = Logger.getRootLogger();

	public static String login(HttpServletRequest req) {

 
		String username = req.getParameter("username");
		String password = req.getParameter("password");

		User u = UserService.getUserByUsername(username);
		int id = u.getUserId();

		
		if (UserService.validateLogin(username, password)) {
			CurrentUser.currentUser = u;
			
			HttpSession session = req.getSession();
			session.setAttribute("username", username);
			session.setAttribute("userId", id);
//			System.out.println(id);
			
			if (u.getRoleId() == 1) {
				log.info("User logged in as an employee.");
				return "employee.html";
			} else if (u.getRoleId() == 2) {
				log.info("User logged in as a manager.");
				return "manager.html";
			}
		}

		else {
			log.info("Invalid login information.");
			return "login.html";
		}
		return "login.html";
	}
	
	public static String logout(HttpServletRequest req) {
		
		HttpSession session = req.getSession();
		session.invalidate();
		log.info("Successfully logged out.");
		System.out.println("Is this working?");
		
		return "login.html";
		
	}
}
